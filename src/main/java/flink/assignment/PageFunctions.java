package flink.assignment;

import common.BaseAutomation;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class PageFunctions extends BaseAutomation {
	public static By temperature = By.xpath("//span[@id='temperature']");
	public static By btn_moisturizers = By.xpath("//button[text()='Buy moisturizers']");
	public static By btn_sunscreens = By.xpath("//button[text()='Buy sunscreens']");
	public static By spf30 = By.xpath("//p[contains(text(),'-30')]");
	public static By spf50 = By.xpath("//p[contains(text(),'-50')]");
	public static By maxPriceSpf50 = By.xpath("(//p[contains(text(),'-50')])[${value}]/following-sibling::p");
	public static By maxPriceSpf50ProductName = By.xpath("(//p[contains(text(),'-50')])[${value}]");
	public static By minPriceSpf30 = By.xpath("(//p[contains(text(),'-30')])[${value}]/following-sibling::p");
	public static By minPriceSpf30ProductName = By.xpath("(//p[contains(text(),'-30')])[${value}]");
	public static By btn_add = By.xpath("//button[contains(@onclick,'${productName}') and text()='Add']");
	public static By btn_cart = By.xpath("//button[@onclick='goToCart()']");
	public static By btn_cartValue = By.xpath("//span[@id='cart']");	
	public static By cartProductsCountRow = By.xpath("//table[@class='table table-striped']//tr");
	public static By cartProductsCountCoulum = By.xpath("//table[@class='table table-striped']//tr[${value}]//td");
	public static By cartProductsValue = By.xpath("//table[@class='table table-striped']//tr[${value}]//td[${value2}]");
	public static By btn_payment = By.xpath("//span[text()='Pay with Card']/parent::button[@type='submit']");
	public static By iFrame_PaymentLayer = By.xpath("//iframe[@name='stripe_checkout_app']");
	public static By emailAddress = By.id("email");
	public static By cardNumber = By.id("card_number");
	public static By validUpto = By.id("cc-exp");
	public static By cvc = By.id("cc-csc");
	public static By zipCode = By.id("billing-zip");
	public static By btn_gatewayPayment = By.id("submitButton");
	public static By paymentStatus = By.xpath("//p[contains(text(),'${value}')]");
	public static By aloeMoisturizers = By.xpath("//p[contains(text(),'Aloe')]");
	public static By aloeMoisturizersPrice = By.xpath("(//p[contains(text(),'Aloe')])[${value}]/following-sibling::p");
	public static By aloeMoisturizersName = By.xpath("(//p[contains(text(),'Aloe')])[${value}]");
	public static By almondMoisturizers = By
			.xpath("//p[contains(translate(.,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz'),'almond')]");
	public static By almondMoisturizersPrice = By.xpath(
			"(//p[contains(translate(.,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz'),'almond')])[${value}]/following-sibling::p");
	public static By almondMoisturizersName = By.xpath(
			"(//p[contains(translate(.,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz'),'almond')])[${value}]");

	public PageFunctions(WebDriver driver) {
		super(driver);
	}

	public void openApplicationHomePage(String url) {
		goToPageURL(url);
	}
	public void openApplication(String url) {
		goToPageURL(url);
	}
	/**
	 * This method is used for selection shopping according to weather
	 */
	public String shoppingSelectionAsPerTemperature() {
		String product = "";
		int temperatureValue = returnIntegerValueFromString(getText(temperature));
		if (temperatureValue < 19) {
			click(btn_moisturizers);
			product = "moisturizers";
		} else if (temperatureValue > 34) {
			click(btn_sunscreens);
			product = "sunscreens";
		}
		return product;
	}

	/**
	 * This method is used for add minimum price sunscreen in cart
	 * 
	 * @return
	 */
	public Map<String, Integer> addMinPriceSunScreen() {
		String productName = "";
		Map<String, Integer> hm = getMinPriceProduct(spf30, minPriceSpf30, minPriceSpf30ProductName);
		for (Map.Entry<String, Integer> m : hm.entrySet())
			productName = m.getKey();
		addProduct(productName);
		return hm;
	}

	/**
	 * This method is used for add maximum price sunscreen in cart
	 * 
	 * @return
	 */
	public Map<String, Integer> addMaxPriceSunScreen() {
		String productName = "";
		Map<String, Integer> hm = getMaxPriceProduct(spf50, maxPriceSpf50, maxPriceSpf50ProductName);
		for (Map.Entry<String, Integer> m : hm.entrySet())
			productName = m.getKey();
		addProduct(productName);
		return hm;
	}

	/**
	 * This method is used for add minimum price Moisturizer in cart
	 * 
	 * @return
	 */
	public Map<String, Integer> addMinPriceMoisturizers() {
		String productName = "";
		Map<String, Integer> hm = getMinPriceProduct(almondMoisturizers, almondMoisturizersPrice,
				almondMoisturizersName);
		for (Map.Entry<String, Integer> m : hm.entrySet())
			productName = m.getKey();
		addProduct(productName);
		return hm;
	}

	/**
	 * This method is used for add maximum price Moisturizer in cart
	 * 
	 * @return
	 */
	public Map<String, Integer> addMaxPriceMoisturizers() {
		String productName = "";
		Map<String, Integer> hm = getMaxPriceProduct(aloeMoisturizers, aloeMoisturizersPrice, aloeMoisturizersName);
		for (Map.Entry<String, Integer> m : hm.entrySet())
			productName = m.getKey();
		addProduct(productName);
		return hm;
	}

	public Map<String, Integer> addProductToCart(String product) {
		Map<String, Integer> hm;
		Map<String, Integer> hm1;
		if (product.equals("moisturizers")) {
			hm = addMaxPriceMoisturizers();
			hm1 = addMinPriceMoisturizers();
			hm.putAll(hm1);
		} else {
			hm = addMaxPriceSunScreen();
			hm1 = addMinPriceSunScreen();
			hm.putAll(hm1);
		}
		return hm;
	}

	/**
	 * By this metthod we are getting minimum product name and price
	 * 
	 * @param byLocator1
	 * @param byLocator2
	 * @param byLocator3
	 * @return
	 */
	public Map<String, Integer> getMinPriceProduct(By byLocator1, By byLocator2, By byLocator3) {
		Map<String, Integer> hm = new HashMap<String, Integer>();
		int minPrice = Integer.MAX_VALUE;
		String minPriceProduct = "";
		int totalProductCount = getElements(byLocator1).size();
		for (int i = 1; i <= totalProductCount; i++) {
			int productValue = returnIntegerValueFromString(
					getText(parameterizedLocator(byLocator2, String.valueOf(i))));
			if (minPrice > productValue) {
				minPrice = productValue;
				minPriceProduct = getText(parameterizedLocator(byLocator3, String.valueOf(i)));
			}
		}
		hm.put(minPriceProduct, minPrice);
		return hm;
	}

	/**
	 * By this metthod we are getting maximum product name and price
	 * 
	 * @param byLocator1
	 * @param byLocator2
	 * @param byLocator3
	 * @return
	 */
	public Map<String, Integer> getMaxPriceProduct(By byLocator1, By byLocator2, By byLocator3) {
		Map<String, Integer> hm = new HashMap<String, Integer>();
		int maxPrice = Integer.MIN_VALUE;
		String maxPriceProduct = "";
		int totalProductCount = getElements(byLocator1).size();
		for (int i = 1; i <= totalProductCount; i++) {
			int productValue = returnIntegerValueFromString(
					getText(parameterizedLocator(byLocator2, String.valueOf(i))));
			if (maxPrice < productValue) {
				maxPrice = productValue;
				maxPriceProduct = getText(parameterizedLocator(byLocator3, String.valueOf(i)));
			}
		}
		hm.put(maxPriceProduct, maxPrice);
		return hm;
	}

	/**
	 * This method will used for adding product in card
	 * 
	 * @param productName
	 */
	public void addProduct(String productName) {
		click(parameterizedLocator(btn_add, productName));
	}

	/**
	 * This method will click on cart button
	 */
	public void clickOnCart() {
		click(btn_cart);
	}

	/**
	 * This product will get the added product list in cart
	 * 
	 * @return
	 */
	public Map<String, Integer> getCartProduct() {
		ArrayList<Object> al = new ArrayList<>();
		int productRowCount = getElements(cartProductsCountRow).size();
		for (int i = 1; i < productRowCount; i++) {
			int productColumnCount = getElements(parameterizedLocator(cartProductsCountCoulum, String.valueOf(i)))
					.size();
			for (int j = 1; j <= productColumnCount; j++) {
				String productValue = getText(
						parameterizedLocator(cartProductsValue, String.valueOf(i), String.valueOf(j)));
				al.add(productValue);	
			}
		}
		Map<String, Integer> cartProduct = new HashMap<String, Integer>();
		for(int i=0;i<al.size()-1;i+=2) {
			cartProduct.put(String.valueOf(al.get(i)), Integer.valueOf((String)al.get(i+1)));
		}
		return cartProduct;
	}

	/**
	 * This method will click on payment button
	 */
	public void clickOnPaymentButton() {
		click(btn_payment);
	}

	/**
	 * This method will fill card details
	 */
	public void fillPaymentDetails(String email, String cardNumberValue, String validUptoValue, String cvcValue, String zipCodeValue) {
		switchToFrame(iFrame_PaymentLayer);
		sendKeys(emailAddress, email);
		sendkeysAction(cardNumber, cardNumberValue);
		sendkeysAction(validUpto, validUptoValue);
		sendKeys(cvc, cvcValue);
		sendKeys(zipCode,zipCodeValue);
		click(btn_gatewayPayment);
		switchToDefaultContent();
	}

	/**
	 * This method will verify the payment status
	 * 
	 * @return
	 */
	public boolean verifyPaymentSuccess(String Status) {
		return isDisplayed(parameterizedLocator(paymentStatus, Status));
	}

	/**
	 * This method will verify cart product
	 * 
	 * @param map
	 * @return
	 */
	public boolean verifyCartProduct(Map<String, Integer> map) {
		return map.equals(getCartProduct());
	}

}
