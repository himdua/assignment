package common;

import java.util.HashMap;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterSuite;

public class DriverFactory {

	private DriverFactory factory;
	private WebDriver webDriver;

	public DriverFactory() {
	}


	public DriverFactory getInstance() {
		if (factory==null)
			factory = new DriverFactory();
		return factory;
	}

	/***
	 * Builds a web driver instance based on the the configured browser and the OS
	 **/
	public WebDriver buildWebDriver() {
		//WebDriver webDriver;
		String OSNAme = System.getProperty("os.name").toLowerCase();
		switch (FlinkContext.SINGLETON.getEntryAsString("browserName")) {
		case "Firefox":
			webDriver = makeFirefoxDriver(OSNAme); // Firefox runs on all OSs
			break;
		case "Chrome":
			webDriver = makeChromeDriver(OSNAme);
			break;
		default:
			throw new RuntimeException("Browser not supported");
		}
		return webDriver;
	}

	private WebDriver makeChromeDriver(String OSName) {
		if (OSName.contains("win")) {
			HashMap<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("profile.default_content_setting_values.notifications", 2);
			ChromeOptions options = new ChromeOptions();
			options.setExperimentalOption("prefs", prefs);
			System.setProperty("webdriver.chrome.driver", "drivers/win/chromedriver.exe");
		} else if (OSName.contains("mac"))
			System.setProperty("webdriver.chrome.driver", "drivers/mac/chromedriver");
		else
			System.setProperty("webdriver.chrome.driver", "drivers/unix/chromedriver");
		return new ChromeDriver();
	}

	private WebDriver makeFirefoxDriver(String OSName) {
		DesiredCapabilities capa = DesiredCapabilities.firefox();
		capa.setCapability("marionette", false);
		FirefoxOptions fp = new FirefoxOptions();
		fp.setAcceptInsecureCerts(true);
		fp.addPreference("security.enable_java", true);
		fp.addPreference("plugin.state.java", 2);
		fp.addPreference("media.navigator.permission.disabled", true);
		if (OSName.contains("win"))
			System.setProperty("webdriver.gecko.driver", "drivers/win/geckodriver.exe");
		else if (OSName.contains("mac"))
			System.setProperty("webdriver.gecko.driver", "drivers/mac/geckodriver");
		else
			System.setProperty("webdriver.gecko.driver", "drivers/unix/geckodriver");
		capa.setCapability("marionette", true);
		System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "/dev/null");

		WebDriver d = new FirefoxDriver(fp);
		return d;
	}

	public void configBrowser(WebDriver driver){
		BaseAutomation baseAutomation= new BaseAutomation(driver);
	}

	public WebDriver getDriver(){
		return webDriver;
	}

	private static ThreadLocal<WebDriver> driveList = new ThreadLocal<WebDriver>();

	public static WebDriver getWebDriver() {
		return driveList.get();
	}

	public static void setWebDriver(WebDriver driver) {
		driveList.set(driver);
	}
}
