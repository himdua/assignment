package common;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.*;

import java.io.IOException;
import java.util.Properties;
import java.util.Set;

public class DriverTest {

	protected Properties classProperties;

	protected Properties moduleProperties;
	private DriverFactory df= new DriverFactory();
	@BeforeSuite
	public void initializeSuite() throws IOException {
		Reporter.log("Starting Suite ..... ", true);
		Properties suiteProperties = new Properties();
		suiteProperties.load(this.getClass().getClassLoader().getResourceAsStream("run.properties"));
		FlinkContext.SINGLETON.loadFromProperties(suiteProperties);
		Reporter.log("Test Suite Initialized with the following properties", true);
		Reporter.log("****************************************************", true);
		Set<String> keys = FlinkContext.SINGLETON.getEntryKeys();
		for (String aKey : keys) {
			String str = "* " + aKey + " -> " + FlinkContext.SINGLETON.getEntry(aKey);
			Reporter.log(str, true);
		}
		Reporter.log("Starting drivers ..... ", true);
		//WebDriver driver=df.getInstance().buildWebDriver();
		//df.getInstance().configBrowser(driver);
	}

	@BeforeTest
	public void loadModuleProperties() {
		moduleProperties = new Properties();
		try {
			Reporter.log("Loading Module Properties...");
			moduleProperties.load(this.getClass().getClassLoader().getResourceAsStream("module.properties"));
		} catch (Exception e) {
			Reporter.log("No Module Properties to load");
		}

	}

	// Return Module/Class Property Key - Value
	protected String getTestProperty(String key) {
		return moduleProperties != null ? moduleProperties.getProperty(key) != null ? moduleProperties.getProperty(key)
				: classProperties.getProperty(key) : classProperties.getProperty(key);

	}
	@BeforeClass
	public void loadClassProperty() {
		classProperties = new Properties();
		try {
			classProperties.load(this.getClass().getClassLoader()
					.getResourceAsStream(this.getClass().getSimpleName()));
		} catch (Exception e) {
			Reporter.log("No Class Properties to Load", true);
		}
	}
	@AfterClass
	public void close(){
		//df.getInstance().getDriver().quit();
	}
}
