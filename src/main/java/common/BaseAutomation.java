package common;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;


public class BaseAutomation {
	private WebDriver driver;
	private WebDriverWait wait;

	private final int EXPLICIT_WAIT_TIME = 10;
	private final int IMPLICIT_WAIT_TIME = 30;

	public BaseAutomation(WebDriver driver) {
		this.driver = driver;
		this.driver.manage().window().maximize();
		wait = new WebDriverWait(this.driver, EXPLICIT_WAIT_TIME);
	}
	public WebElement getElement(By by) {
		return getElement(by, "");
	}

	public void goToPageURL(String stringURL) {
		driver.get(stringURL);
		waitForPageToLoadCompletely();
	}

	public List<WebElement> getElements(By by) {
		return getElements(by, "");
	}

	public List<WebElement> getElements(By by, String replacementValue) {
		List<WebElement> elements;
		try {
			by = parameterizedLocator(by, replacementValue);
			elements = driver.findElements(by);
		} catch (StaleElementReferenceException | NoSuchElementException e) {
			elements = driver.findElements(by);
		}
		return elements;
	}

	public WebElement getElement(By by, String replacementValue) {
		WebElement element;
		try {
			by = parameterizedLocator(by, replacementValue);
			element = waitForVisibilityOfElement(by);
		} catch (StaleElementReferenceException | NoSuchElementException e) {
			element = waitForVisibilityOfElement(by);
		}
		return element;
	}

	protected By parameterizedLocator(By by, String param) {
		if (!param.isEmpty()) {
			String loc = by.toString().replaceAll("\'", "\\\"");
			String type = loc.split(":", 2)[0].split(",")[0].split("\\.")[1];
			String variable = loc.split(":", 2)[1].replaceFirst("\\$\\{.+?}", param);
			return getBy(type, variable);
		} else {
			return by;
		}
	}

	protected By parameterizedLocator(By by, String param1, String param2) {
		if (!param1.isEmpty()) {
			by = parameterizedLocator(by, param1);
		}
		if (!param2.isEmpty()) {
			by = parameterizedLocator(by, param2);
		}
		return by;
	}

	protected void resetImplicitTimeout(int newTimeOut) {
		driver.manage().timeouts().implicitlyWait(newTimeOut, TimeUnit.SECONDS);
	}

	protected By getBy(String locatorType, String locatorValue) {
		switch (locatorType) {
		case "id":
			return By.id(locatorValue);
		case "xpath":
			return By.xpath(locatorValue);
		case "css":
		case "cssSelector":
			return By.cssSelector(locatorValue);
		case "name":
			return By.name(locatorValue);
		case "classname":
			return By.className(locatorValue);

		case "linktext":
			return By.linkText(locatorValue);
		default:
			return By.id(locatorValue);
		}
	}

	protected void click(By by) {
		WebElement element;
		try {
			element = waitForElementToBeClickable(by);
			element.click();
		} catch (StaleElementReferenceException se) {
			element = waitForElementToBeClickable(by);
			this.scrollDown(by);
			element.click();
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	protected void sendKeys(By by, String stringText) {
		WebElement webelement;
		try {
			webelement = waitForVisibilityOfElement(by);
			webelement.click();
			webelement.clear();
			webelement.sendKeys(stringText);
		} catch (StaleElementReferenceException se) {
			webelement = waitForVisibilityOfElement(by);
			this.scrollDown(by);
			webelement.click();
			webelement.clear();

			webelement.sendKeys(stringText);
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	protected String getText(By by) {
		String value = null;
		WebElement element;
		try {
			element = waitForVisibilityOfElement(by);
			value = element.getText();
		} catch (StaleElementReferenceException e) {
			element = waitForVisibilityOfElement(by);
			this.scrollDown(by);
			value = element.getText();
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
		return value;
	}

	protected WebElement waitForVisibilityOfElement(By locator) {
		WebElement element;
		try {
			element = wait.until(ExpectedConditions.presenceOfElementLocated(locator));
			element = wait.until(ExpectedConditions.visibilityOf(element));
		} catch (TimeoutException | NoSuchElementException se) {
			try {
				element = wait.until(ExpectedConditions.presenceOfElementLocated(locator));
			} catch (Exception e) {

				element = null;
				Assert.fail(e.getMessage());
			}
		}
		return element;
	}

	protected WebElement waitForElementToBeClickable(By locator) {
		waitForVisibilityOfElement(locator);
		WebElement element;
		element = wait.until(ExpectedConditions.elementToBeClickable(locator));
		return element;
	}

	protected void waitForPageToLoadCompletely() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*")));
	}

	protected void scrollDown(By by) {
		WebElement element = waitForVisibilityOfElement(by);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
	}

	public void hardWait(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException ex) {
			ex.printStackTrace();
		}
	}

	protected void switchToFrame(By by) {
		WebElement element;
		try {
			element = wait.until(ExpectedConditions.presenceOfElementLocated(by));
			driver.switchTo().frame(element);
		} catch (Exception e) {

			element = null;
			Assert.fail(e.getMessage());
		}
	}

	protected void switchToDefaultContent() {
		driver.switchTo().defaultContent();
		hardWait(2);
		Reporter.log("Switching to default content...");
	}

	protected boolean isDisplayed(By by) {
		boolean flag = true;
		try {
			resetImplicitTimeout(1);
			waitForVisibilityOfElement(by);
			try {
				waitForVisibilityOfElement(by);
				if (!driver.findElement(by).isDisplayed()) {
					flag = false;
				}
			} catch (StaleElementReferenceException e) {
				waitForVisibilityOfElement(by);
				if (!driver.findElement(by).isDisplayed()) {
					flag = false;
				}
			} catch (Exception e) {
				flag = false;
			}
			return flag;
		} catch (TimeoutException te) {
			Reporter.log("Element is not displayed");
			return false;
		} finally {
			resetImplicitTimeout(IMPLICIT_WAIT_TIME);
		}
	}

	public int returnIntegerValueFromString(String value) {
		return Integer.valueOf(value.replaceAll("[^-?0-9]+", " ").trim());
	}

	public void sendkeysAction(By by, String stringText){
		Actions action = new Actions(this.driver);
		WebElement webelement;
		try {
			webelement = waitForVisibilityOfElement(by);
			webelement.click();
			webelement.clear();
			action.sendKeys(webelement, stringText).build().perform();
		} catch (StaleElementReferenceException se) {
			webelement = waitForVisibilityOfElement(by);
			this.scrollDown(by);
			webelement.click();
			webelement.clear();
			action.sendKeys(webelement, stringText).build().perform();
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
}
