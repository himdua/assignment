package script;

import common.DriverFactory;
import common.DriverTest;
import flink.assignment.PageFunctions;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Map;

public class ParallelExecutionClass extends DriverTest {

    private PageFunctions pageFunctions;
    private DriverFactory df;
    WebDriver driver;

    @BeforeMethod
    public void initializeObject() {
        df = new DriverFactory();
        driver = df.getInstance().buildWebDriver();
        df.setWebDriver(driver);
        pageFunctions = new PageFunctions(df.getWebDriver());
    }

    @Test
    public void openApplication() {
        pageFunctions.openApplicationHomePage(getTestProperty("URL1"));
    }

    @Test
    public void openApplication_another() {
        pageFunctions.openApplicationHomePage(getTestProperty("URL"));
    }

   @AfterMethod
    public void closeBrowser() {
        System.out.println(driver.toString());
        df.getWebDriver().quit();
    }
}
