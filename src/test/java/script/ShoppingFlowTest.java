package script;

import common.DriverFactory;
import flink.assignment.PageFunctions;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import common.DriverTest;

import java.util.Map;

public class ShoppingFlowTest extends DriverTest {

    private PageFunctions pageFunctions;
    private DriverFactory df;
    WebDriver driver;

    @BeforeMethod
    public void initializeObject() {
        df = new DriverFactory();
        driver = df.getInstance().buildWebDriver();
        df.setWebDriver(driver);
        pageFunctions = new PageFunctions(df.getWebDriver());
    }

    @Test
    public void shopSkinProducts() {
        pageFunctions.openApplicationHomePage(getTestProperty("URL"));
        String product = pageFunctions.shoppingSelectionAsPerTemperature();
        Map<String, Integer> hm = pageFunctions.addProductToCart(product);
        pageFunctions.clickOnCart();
        Assert.assertTrue(pageFunctions.verifyCartProduct(hm), "Added Cart Products are not same as added.");
        pageFunctions.clickOnPaymentButton();
        pageFunctions.fillPaymentDetails(getTestProperty("email"), getTestProperty("cardNumber"), getTestProperty("validUpto"), getTestProperty("cvc"), getTestProperty("zipCode"));
        Assert.assertTrue(pageFunctions.verifyPaymentSuccess(getTestProperty("status")), "Payment is not successfully done., Please try again after some time");
    }

   @AfterMethod
    public void closeBrowser() {
        System.out.println(driver.toString());
        df.getWebDriver().quit();
    }
}
