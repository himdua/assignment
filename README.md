# Flink Assignment
Pre-requisite:
1. Java should be installed and path should be set in environment veriable
2. Maven should be installed and path should be set in environment veriable
3. Firefox should be installed with version 95.0.2 as we have added geckodriver with this compatibility only
4. Chrome should be installed with version 96.0.4664.45 as we have added chromedriver with this compatibility only


Running Command
1. Open Command Prompt on windows
2. hit command for Firefox --> mvn test -Denvironment=prod -Dbrowser=Firefox -Dtest=ShoppingFlowTest
3. hit command for Chrome --> mvn test -Denvironment=prod -Dbrowser=Chrome -Dtest=ShoppingFlowTest
4. Hit command for parallel execution--> mvn test -Denvironment=prod -Dbrowser=Chrome -Dsuite=textng.xml